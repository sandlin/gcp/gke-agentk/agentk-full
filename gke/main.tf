/*
* Create Network for our TF cluster.
*/

provider "google" {
  project     = var.project
}

resource "random_pet" "name" {
  length = 1
}

locals {
  //cluster_name = "${var.prefix}-${random_pet.name.id}"
  cluster_name = "${var.prefix}-robin"
}
/*
* Create GKE Cluster & grant bindings required for internal management.
*/

# 2021-12-22T13:17:11.264-0800 [WARN]  Provider "provider[\"registry.terraform.io/hashicorp/google\"]" produced an unexpected new value for google_container_node_pool.primary_nodes, but we are tolerating it because it is using the legacy plugin SDK.
#     The following problems may be the cause of any confusing errors from downstream operations:
#       - .node_config[0].min_cpu_platform: was null, but now cty.StringVal("")
#       - .node_config[0].node_group: was null, but now cty.StringVal("")
      # - .subject[0].namespace: planned value cty.StringVal("default") for a non-computed attribute
      # - .subject[1].namespace: planned value cty.StringVal("default") for a non-computed attribute
# 2021-12-22T13:17:11.264-0800 [TRACE] NodeAbstractResouceInstance.writeResourceInstanceState to workingState for google_container_node_pool.

# Separately Managed Node Pool
resource "google_container_cluster" "primary" {
  name               = local.cluster_name
  location           = var.zone
  initial_node_count = 1
  node_config {
    # Google recommends custom service accounts that have cloud-platform scope and permissions granted via IAM Roles.
    service_account = var.cluster_sa_email
    oauth_scopes = [
      "https://www.googleapis.com/auth/cloud-platform",
      "https://www.googleapis.com/auth/compute",
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]
    labels = {
      env = var.project
      owner = "jsandlin"
    }
    machine_type = "n1-standard-1"
    tags         = ["${local.cluster_name}-node", "${var.project}-gke"]
  }
  timeouts {
    create = "30m"
    update = "40m"
  }
}