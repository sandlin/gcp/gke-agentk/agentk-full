# Your Google Cloud settings
project = "jsandlin-c9fe7132"
region  = "us-west1"
zone    = "us-west1-a"

# Network information
subnet = "greenscar-skylark-subnet"
vpc = "greenscar-skylark-vpc"

# Service Account Information
cluster_sa_email = "greenscar-elephant-sa@jsandlin-c9fe7132.iam.gserviceaccount.com"
cluster_sa_id = "113737407820475206197"

# Naming prefix
prefix = "greenscar"

