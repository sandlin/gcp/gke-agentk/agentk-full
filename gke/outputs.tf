
output "cluster_name" {
  value       = google_container_cluster.primary.name
  description = "GKE Cluster Name"
}

# output "kubernetes_cluster_host" {
#   value       = google_container_cluster.primary.endpoint
#   description = "GKE Cluster Host"
# }

# output "kubernetes_server_defined_url" {
#   value = google_container_cluster.primary.self_link
#   description = "The server-defined URL for the resource."
# }

output "gcloud_config_cmd" {
  value = "gcloud container clusters get-credentials ${google_container_cluster.primary.name} --region ${var.region}"
  description = "GCloud cmd to run to download your cluster creds"
}