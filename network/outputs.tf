output "vpc" {
    value = google_compute_network.vpc.name
    description = "GCP VPC Name"
}
output "subnet" {
    value = google_compute_subnetwork.subnet.name
    description = "GCP Subnet Name"
}

output "firewall" {
    value = google_compute_firewall.firewall.name
    description = "GCP Firewall Name"
}