# Your Google Cloud settings
project = "jsandlin-c9fe7132"
region  = "us-west1"
zone    = "us-west1-a"

# Network information
subnet_cidr   = "10.1.1.0/24"

# Naming prefix
prefix = "greenscar"