/*
* Create Network for our TF cluster.
*/

provider "google" {
  project     = var.project
}


resource "random_pet" "name" {
  length = 1
}

locals {
  prefix = "${var.prefix}-${random_pet.name.id}"
}

// Create VPC
resource "google_compute_network" "vpc" {
 name                    = "${local.prefix}-vpc"
 auto_create_subnetworks = "false"
}

// Create Subnet
resource "google_compute_subnetwork" "subnet" {
 name          = "${local.prefix}-subnet"
 ip_cidr_range = var.subnet_cidr
 network       = "${local.prefix}-vpc"
 depends_on    = [google_compute_network.vpc]
 region      = var.region
}

// VPC firewall configuration
resource "google_compute_firewall" "firewall" {
  name    = "${local.prefix}-firewall"
  network = "${google_compute_network.vpc.name}"

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["22", "443"]
  }

  source_ranges = ["0.0.0.0/0"]
  depends_on    = [google_compute_network.vpc]
}
