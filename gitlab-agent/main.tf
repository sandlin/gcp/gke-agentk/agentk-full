# https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/cluster_agent_token#example-usage
terraform {
  required_providers {
    kubernetes = "~> 2.3"
    kubectl = {
      source  = "gavinbunney/kubectl"
      version = "~> 1.14"
    }
    graphql = {
      source = "sullivtr/graphql"
    }
    gitlab = {
      source = "gitlabhq/gitlab"
      version = "~>3.20.0"
    }
  }
}

provider "helm" {
  kubernetes {
    config_path = "~/.kube/config"
  }
}

provider "gitlab" {
    token = var.gitlab_token
}

provider "google" {
  project     = var.project
}


# Retrieve an access token as the Terraform runner
data "google_client_config" "provider" {}

data "google_container_cluster" "my_cluster" {
  name     = var.cluster_name
  project = var.project
  location = var.region
}


# Get the Service Account Token we will use to control Kubernetes.
data "google_service_account_access_token" "sa_token" {
  provider               = google
  target_service_account = var.cluster_sa_email
  scopes                 = ["cloud-platform"]
  lifetime               = "300s"
}

# Setup provider
provider "kubernetes" {
  host  = "https://${data.google_container_cluster.my_cluster.endpoint}"
  token = data.google_service_account_access_token.sa_token.access_token
  cluster_ca_certificate = base64decode(
    data.google_container_cluster.my_cluster.master_auth[0].cluster_ca_certificate,
  )
}

# Register an agent with GitLab Project.
# https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/cluster_agent
resource "gitlab_cluster_agent" "gitlab-agent-registration" {
  project = var.gitlab_project
  name = var.agent_name
}

# Now install the agent in the cluster.
# https://registry.terraform.io/providers/gitlabhq/gitlab/latest/docs/resources/cluster_agent_token#example-usage
resource "gitlab_cluster_agent_token" "agent" {
  project     = var.gitlab_project
  agent_id    = gitlab_cluster_agent.gitlab-agent-registration.agent_id
  name        = "gitlab-agent-token"
  description = "Token for the my-agent used with `gitlab-agent` Helm Chart"
}

resource "helm_release" "gitlab_agent" {
  name             = "gitlab-agent"
  namespace        = "gitlab-agent"
  create_namespace = true
  repository       = "https://charts.gitlab.io"
  chart            = "gitlab-agent"
  version          = var.helm_chart_version

  set {
    name  = "config.token"
    value = gitlab_cluster_agent_token.agent.token
  }
}