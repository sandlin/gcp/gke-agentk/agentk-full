
output "glagent_agent_id"{
    value = gitlab_cluster_agent.gitlab-agent-registration.agent_id
}
output "glagent_id"{
    value = gitlab_cluster_agent.gitlab-agent-registration.id
}
output "glagent_name"{
    value = gitlab_cluster_agent.gitlab-agent-registration.name
}
output "glagent_project"{
    value = gitlab_cluster_agent.gitlab-agent-registration.project
}