
# Store state in the project.
terraform {
  required_version = ">=1.0.0"
  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/40487813/terraform/state/agentk-agent/"
    lock_address   = "https://gitlab.com/api/v4/projects/40487813/terraform/state/agentk-agent/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/40487813/terraform/state/agentk-agent/lock"
    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = 5
  }
}
