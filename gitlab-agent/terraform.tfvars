# Your Google Cloud settings
project = "jsandlin-c9fe7132"
region  = "us-west1"
zone    = "us-west1-a"
#prefix = "greenscar"

# Service Account Information

cluster_sa_email = "greenscar-elephant-sa@jsandlin-c9fe7132.iam.gserviceaccount.com"
cluster_sa_id = "113737407820475206197"

cluster_name = "greenscar-robin"
agent_name = "greenscar-robin-agent"

# The project holding the GitLab Agent configs. 
# For this project, the value is the ID of this project.

gitlab_project = 41500656 # https://gitlab.com/sandlin/gcp/gke-agentk/agentk-registration

# https://gitlab.com/gitlab-org/charts/gitlab-agent/-/tags
helm_chart_version = "1.9.2"