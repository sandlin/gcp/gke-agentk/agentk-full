variable "project" {
  description = "The GCP Project"
  type = string
}

variable "region" {
  description = "The GCP Region"
  type = string
}

variable "zone" {
  description = "The GCP Zone"
  type = string
}

variable "cluster_name" {
  description = "GKE Cluster Name"
  type = string
}

variable "agent_name" {
  description = "GitLab Agent Name"
  type = string
  default = "gitlab-agent"
}

variable "cluster_sa_id" {
    description = "The ID of the cluster management service account."
    type = string
}

variable "cluster_sa_email" {
    description = "The email of the cluster management service account."
    type = string
}

variable "gitlab_token" {
  type = string
  default     = ""
}

variable "gitlab_project" {
  description = "ID or full path of the project maintained by the authenticated user."
  type = string
}

# https://gitlab.com/gitlab-org/charts/gitlab-agent/-/tags
variable "helm_chart_version" {
  description = "Version of GitLab-Agent Helm Chart"
  type = string
}