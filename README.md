# agentk-prereq

## Overview
This instruction set is written to guide you through the entire process involved in setting up and integrating a Kubernetes Cluster with GitLab. The official documentation is available on GitLab under [GitLab Agent for Kubernetes](https://docs.gitlab.com/ee/user/clusters/agent/)

- The `.gitlab-ci.yml` of this repo is configured to pull the Google Cloud Service Account JSON and the GitLab Service Account token from Hashicorp Vault.
- In each directory, there is a `install.sh` file intended to make the TF calls easier from local.

---

## Pre-Requisites

### Google Cloud

##### Required Software
1. [Google Cloud SDK (gcloud CLI)](https://cloud.google.com/sdk/docs/install)
  1. GKE v1.26 introduced a new means of auth.
     ```gcloud components install gke-gcloud-auth-plugin```
1. [Terraform CLI](https://learn.hashicorp.com/tutorials/terraform/install-cli)
1. [JQ](https://stedolan.github.io/jq/download/)
1. [Helm](https://helm.sh)

##### GCP Project
1. Take note of your Google Cloud Project ID. Moving forward, this will be referred to as `${GCP_PROJECT_ID}`.

### GitLab Access Token
A GitLab token will be needed in order to call the agent registration endpoints. 
1. Create a [Personal Access Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html)
   - Under `Select scopes`, select `api` and `read_registry`
   - Save this token in your password management tool. It will not be accessable after this page is closed.
1. Create a local file `~/.gitlab/.creds` using this `Access Token`.
    ```
    # GitLab API token
    username="jsandlin"
    password="glpat-QerShxqUsPcAvfAp3xN3P"
    ```

---
## You will create 3 repositories for this effort: 
1) A clone of this project (agentk-full)[https://gitlab.com/sandlin/gcp/gke-agentk/agentk-full]
  - This houses the TF which will create the infrastructure & setup the GitLab Agent for Kubernetes.
2) A project under which your agent will register. EX: (agentk-registration)[https://gitlab.com/sandlin/gcp/gke-agentk/agentk-registration] - ref: (#create-agent-registration-project)
3) A cluster management project; [agentk-management](https://gitlab.com/sandlin/gcp/gke-agentk/agentk-management) was created via the `GitLab Cluster Management` Template.


## Instructions
1. [Enable required GCP services](#service-enablement)
1. [Create network](#network-creation)
1. [Create service account](#service-account-creation)
1. [Bind service account to IAM policies](#iam-policy-binding)
1. [Create Kubernetes cluster](#kubernetes-cluster-creation)
1. [Point kubectl to the new cluster](#connect-gcloud-to-your-cluster)
1. [Create GitLab project to manage software in your new cluster](#create-agent-registration-project)
1. [Install agent & register it with this project](#gitlab-agent-installation-and-registration)
1. [Create the cluster management project]


--- 

## Service Enablement
1. Execute the shell script & provide your project ID.
   `bash scripts/services_enable.sh -p ${GCP_PROJECT_ID}`
   ex:
   `bash scripts/services_enable.sh -p jsandlin-c9fe7132`

## Network Creation
1. `cd network`
1. [Modify Files](#for-each-project-modify-files)
1. `./install.sh create`
1. . Take note of output. 
   ```
   ...
   Outputs:

   firewall = "greenscar-skylark-firewall"
   subnet = "greenscar-skylark-subnet"
   vpc = "greenscar-skylark-vpc"
   ```

## Service Account Creation
1. `cd service-account`
1. [Modify Files](#for-each-project-modify-files)
1. `./install.sh create`
1. Take note of output. 
   ```
   ...
   Outputs:

   cluster_sa_email = "greenscar-elephant-sa@jsandlin-c9fe7132.iam.gserviceaccount.com"
   cluster_sa_id = "113737407820475206197"
   prefix = "greenscar"
   ```

## IAM Policy Binding
1. Execute the shell script & provide your project ID.
   `bash scripts/iam_policy_binding.sh -p ${GCP_PROJECT_ID} -a ${SERVICE_ACCOUNT}`
   ex:
   `bash scripts/iam_policy_binding.sh -p jsandlin-c9fe7132 -a greenscar-elephant-sa`


## Kubernetes Cluster Creation
- This is where we are going to start using many of the variables from above.
1. `cd gke`
1. [Modify Files](#for-each-project-modify-files)
1. Modify vars in [terraform.tfvars](terraform.tfvars) via output from earlier runs.
1. `./install.sh create`
1. . Take note of output. 
   ```
   ...
   Outputs:

   cluster_name = "greenscar-robin"
   gcloud_config_cmd = "gcloud container clusters get-credentials greenscar-robin --region us-west1"
   ```

## Connect gcloud to your cluster
- This is required to run kubectl and helm locally in order to install the agent.
1. Using the gcloud command above, 
   ```
    gcloud container clusters get-credentials greenscar-robin --region us-west1
    Fetching cluster endpoint and auth data.
    kubeconfig entry generated for greenscar-robin.
   ```

## Cluster Role Binding
### Now we are going to bind the service account to the cluster so it can be the admin user.
1. `cd cluster-role-binding`
1. [Modify Files](#for-each-project-modify-files)
1. Modify vars in [terraform.tfvars](terraform.tfvars) via output from earlier runs.
1. `./install.sh create`

   ```
   ...

   glagent_agent_id = 41658
   glagent_id = "41500656:41658"
   glagent_name = "gitlab-agent"
   glagent_project = "41500656"
   ```

## Create Agent Registration Project
1. Create a project in GitLab which will be the agent connection.
   1. I created [agentk-registration](https://gitlab.com/sandlin/gcp/gke-agentk/agentk-registration)
1. Copy the Project ID so you can populate the agent config.
   1. For this one it is `41500656`

## GitLab Agent Installation and Registration
1. `cd gitlab-agent`
1. [Modify Files](#for-each-project-modify-files)
1. Modify vars in [terraform.tfvars](terraform.tfvars) via output from earlier runs.
1. `./install.sh create`
   ```
   ...
   Outputs:

   glagent_agent_id = 41667
   glagent_id = "41500656:41667"
   glagent_name = "greenscar-robin"
   ```
1. You should now be able to goto your project under which you registered the agent & see the success.
   1. ex: https://gitlab.com/sandlin/gcp/gke-agentk/agentk-registration/-/clusters
1. Take note of the 

   


## Create Cluster Management Project
1. Create a project in GitLab to manage the software in the cluster.
   1. Create a project from Template.
   1. Select `GitLab Cluster Management` Template.


## Manage Cluster Software
1. In the Cluster Management Project, edit the [helmfile](https://gitlab.com/sandlin/gcp/gke-agentk/agentk-management/-/blob/master/helmfile.yaml) to define which services you would like in your cluster.
  - ex:
    ```
    ...
     helmfiles:
       - path: applications/ingress/helmfile.yaml
       - path: applications/cert-manager/helmfile.yaml
    ```
1. In the [.gitlab-ci.yml](https://gitlab.com/sandlin/gcp/gke-agentk/agentk-management/-/blob/master/gitlab-ci.yml) set the `variables/KUBE_CONTEXT` to the value provided when you did your agent registration.
   - ex:
     ```
      ...
      variables:
        KUBE_CONTEXT: sandlin/gcp/gke-agentk/agentk-registration:greenscar-robin
     ```