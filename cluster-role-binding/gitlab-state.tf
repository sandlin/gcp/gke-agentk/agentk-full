
# Store state in the project.
terraform {
  required_version = ">=1.0.0"
  backend "http" {
    address        = "https://gitlab.com/api/v4/projects/40487813/terraform/state/cluster-role-binding/"
    lock_address   = "https://gitlab.com/api/v4/projects/40487813/terraform/state/cluster-role-binding/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/40487813/terraform/state/cluster-role-binding/lock"
    lock_method    = "POST"
    unlock_method  = "DELETE"
    retry_wait_min = 5
  }
}

#variables:
#  TF_ROOT: ${CI_PROJECT_DIR}/${ENVIRONMENT}
#  TF_ADDRESS: ${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/terraform/state/${ENVIRONMENT}