variable "project" {
  description = "The GCP Project"
  type = string
}

variable "region" {
  description = "The GCP Region"
  type = string
}

variable "zone" {
  description = "The GCP Zone"
  type = string
}

variable "cluster_name" {
  description = "GKE Cluster Name"
  type = string
}

variable "prefix" {
  description = "Prefix for names."
  type = string
}
# variable "kubernetes_server_defined_url" {
#   description = "The https endpoint of the cluster."
#   type = "string"
# }



# variable "vpc_name" {
#   description = "Name of VPC within which to create this cluster"
#   type = string
# }

# variable "subnet_name" {
#   description = "Name of Subnet within which to create this cluster"
#   type = string  
# }

# variable "gke_num_nodes" {
#     description = "Number of GKE Nodes in the cluster"
#     default = 2
#     type = number
# }

variable "cluster_sa_id" {
    description = "The ID of the cluster management service account."
    type = string
}

variable "cluster_sa_email" {
    description = "The email of the cluster management service account."
    type = string
}