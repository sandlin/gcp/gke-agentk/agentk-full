
provider "google" {
  project     = var.project
}


# Retrieve an access token as the Terraform runner
data "google_client_config" "provider" {}

data "google_container_cluster" "my_cluster" {
  name     = var.cluster_name
  project = var.project
  location = var.region
}


# Get the Service Account Token we will use to control Kubernetes.
data "google_service_account_access_token" "sa_token" {
  provider               = google
  target_service_account = var.cluster_sa_email
  scopes                 = ["cloud-platform"]
  lifetime               = "300s"
}

# Setup provider
provider "kubernetes" {
  host  = "https://${data.google_container_cluster.my_cluster.endpoint}"
  token = data.google_service_account_access_token.sa_token.access_token
  cluster_ca_certificate = base64decode(
    data.google_container_cluster.my_cluster.master_auth[0].cluster_ca_certificate,
  )
}

# Terraform needs to manage K8S RBAC
# https://cloud.google.com/kubernetes-engine/docs/how-to/role-based-access-control#iam-rolebinding-bootstrap
resource "kubernetes_cluster_role_binding" "terraform_clusteradmin" {
  #provider = kubernetes.k8s-cluster
  metadata {
    name = "cluster-admin-binding-terraform"
  }


  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "cluster-admin"
  }

  # # email: required for gcloud + kubectl using the SA
  # subject {
  #   api_group = "rbac.authorization.k8s.io"
  #   kind      = "User"
  #   name      = google_service_account.sa.email
  # }

  # unique_id: required for Terraform using the SA
  subject {
    api_group = "rbac.authorization.k8s.io"
    kind      = "User"
    name      = var.cluster_sa_id
  }
}

# https://about.gitlab.com/blog/2021/09/10/setting-up-the-k-agent/#service-accounts
# gitlab-agent: Service account used for running agentk

resource "kubernetes_cluster_role" "GitLabClusterMgtRole" {
  #provider = kubernetes.k8s-cluster
  metadata {
    name = "GitLabClusterMgt"
    labels = {
      # Add these permissions to the "admin" and "edit" default roles.
      "rbac.authorization.k8s.io/aggregate-to-admin" = "true"
    }
  }

  # https://about.gitlab.com/blog/2021/09/10/setting-up-the-k-agent/#service-accounts
  rule {
    api_groups = ["*"]
    resources = ["*"]
    verbs = ["get", "list", "watch", "create", "update", "delete", "patch"]
  }
  
  rule {
    # endpoints
    # Error: clusterroles.rbac.authorization.k8s.io "prometheus-prometheus-server" is forbidden: user "system:serviceaccount:gitlab-kubernetes-agent:gitlab-agent" (groups=["system:serviceaccounts" "system:serviceaccounts:gitlab-kubernetes-agent" "system:authenticated"]) is attempting to grant RBAC permissions not currently held:
    #  {NonResourceURLs:["/metrics"], Verbs:["get"]}
    api_groups = [""]
    resources  = ["endpoints"]
    verbs      = ["get", "list", "watch"]
  }
  
  rule {
    # This rule is required for fluentd installation.
    # podsecuritypolicies
    # Error: clusterroles.rbac.authorization.k8s.io "certmanager-cert-manager-controller-approve:cert-manager-io" is forbidden: user "system:serviceaccount:gitlab-kubernetes-agent:gitlab-agent" (groups=["system:serviceaccounts" "system:serviceaccounts:gitlab-kubernetes-agent" "system:authenticated"]) is attempting to grant RBAC permissions not currently held:
    #   {APIGroups:["cert-manager.io"], Resources:["signers"], ResourceNames:["clusterissuers.cert-manager.io/*"], Verbs:["approve"]}
    #   {APIGroups:["cert-manager.io"], Resources:["signers"], ResourceNames:["issuers.cert-manager.io/*"], Verbs:["approve"]}

    # helm.go:88: [debug] clusterroles.rbac.authorization.k8s.io "certmanager-cert-manager-controller-approve:cert-manager-io" is forbidden: user "system:serviceaccount:gitlab-kubernetes-agent:gitlab-agent" (groups=["system:serviceaccounts" "system:serviceaccounts:gitlab-kubernetes-agent" "system:authenticated"]) is attempting to grant RBAC permissions not currently held:
    # {APIGroups:["cert-manager.io"], Resources:["signers"], ResourceNames:["clusterissuers.cert-manager.io/*"], Verbs:["approve"]}
    # {APIGroups:["cert-manager.io"], Resources:["signers"], ResourceNames:["issuers.cert-manager.io/*"], Verbs:["approve"]}

    # Error: clusterroles.rbac.authorization.k8s.io "certmanager-cert-manager-controller-certificatesigningrequests" is forbidden: user "system:serviceaccount:gitlab-kubernetes-agent:gitlab-agent" (groups=["system:serviceaccounts" "system:serviceaccounts:gitlab-kubernetes-agent" "system:authenticated"]) is attempting to grant RBAC permissions not currently held:
    # {APIGroups:["certificates.k8s.io"], Resources:["signers"], ResourceNames:["clusterissuers.cert-manager.io/*"], Verbs:["sign"]}
    # {APIGroups:["certificates.k8s.io"], Resources:["signers"], ResourceNames:["issuers.cert-manager.io/*"], Verbs:["sign"]}
    # helm.go:88: [debug] clusterroles.rbac.authorization.k8s.io "certmanager-cert-manager-controller-certificatesigningrequests" is forbidden: user "system:serviceaccount:gitlab-kubernetes-agent:gitlab-agent" (groups=["system:serviceaccounts" "system:serviceaccounts:gitlab-kubernetes-agent" "system:authenticated"]) is attempting to grant RBAC permissions not currently held:
    # {APIGroups:["certificates.k8s.io"], Resources:["signers"], ResourceNames:["clusterissuers.cert-manager.io/*"], Verbs:["sign"]}
    # {APIGroups:["certificates.k8s.io"], Resources:["signers"], ResourceNames:["issuers.cert-manager.io/*"], Verbs:["sign"]}
    api_groups = ["cert-manager.io", "certificates.k8s.io"]
    resources  = ["signers"]
    verbs      = ["approve", "sign"]
  }
  rule {
    # Error: clusterroles.rbac.authorization.k8s.io "certmanager-cert-manager-edit" is forbidden: user "system:serviceaccount:gitlab-kubernetes-agent:gitlab-agent" (groups=["system:serviceaccounts" "system:serviceaccounts:gitlab-kubernetes-agent" "system:authenticated"]) is attempting to grant RBAC permissions not currently held:
    # {APIGroups:["acme.cert-manager.io"], Resources:["challenges"], Verbs:["deletecollection"]}
    # {APIGroups:["acme.cert-manager.io"], Resources:["orders"], Verbs:["deletecollection"]}
    # {APIGroups:["cert-manager.io"], Resources:["certificaterequests"], Verbs:["deletecollection"]}
    # {APIGroups:["cert-manager.io"], Resources:["certificates"], Verbs:["deletecollection"]}
    # {APIGroups:["cert-manager.io"], Resources:["issuers"], Verbs:["deletecollection"]}
    api_groups = ["acme.cert-manager.io", "cert-manager.io"]
    resources  = ["challenges", "orders", "certificates", "certificaterequests", "issuers"]
    verbs      = ["deletecollection"]
  }
  rule {
    # This rule is required for fluentd installation.
    # podsecuritypolicies
    # Error: roles.rbac.authorization.k8s.io "fluentd" is forbidden: user "system:serviceaccount:gitlab-kubernetes-agent:gitlab-agent" (groups=["system:serviceaccounts" "system:serviceaccounts:gitlab-kubernetes-agent" "system:authenticated"]) is attempting to grant RBAC permissions not currently held:
    #  {APIGroups:["extensions"], Resources:["podsecuritypolicies"], ResourceNames:["fluentd"], Verbs:["use"]}
    api_groups = [""]
    resources  = ["podsecuritypolicies"]
    verbs      = ["get", "list", "watch", "use"]
    
  }
  rule {
    # If you want to use the cluster for GitLab Runners, you need to enable {APIGroups:[""], Resources:["*"], Verbs:["*"]}
    # Error: roles.rbac.authorization.k8s.io "runner-gitlab-runner" is forbidden: user "system:serviceaccount:gitlab-kubernetes-agent:gitlab-agent" (groups=["system:serviceaccounts" "system:serviceaccounts:gitlab-kubernetes-agent" "system:authenticated"]) is attempting to grant RBAC permissions not currently held:
    #   {APIGroups:[""], Resources:["*"], Verbs:["*"]}
    api_groups = [""]
    resources = ["*"]
    verbs = ["*"]
  }
}

resource "kubernetes_cluster_role_binding" "GitLabClusterMgtBinding" {
  #provider = kubernetes.k8s-cluster
  metadata {
    name = "GitLabClusterMgt"
  }
  # role_ref => The ClusterRole to bind subjects to
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind = "ClusterRole"
    name = kubernetes_cluster_role.GitLabClusterMgtRole.metadata[0].name
  }

  # subject => The service account to grant permissions to
  subject {
    api_group = "rbac.authorization.k8s.io"
    kind  = "User"
    name = var.cluster_sa_email
  }
}
