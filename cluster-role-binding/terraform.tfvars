# Your Google Cloud settings
project = "jsandlin-c9fe7132"
region  = "us-west1"
zone    = "us-west1-a"

# Cluster Name
cluster_name = "greenscar-robin"

# Service Account Information
cluster_sa_email = "greenscar-elephant-sa@jsandlin-c9fe7132.iam.gserviceaccount.com"
cluster_sa_id = "113737407820475206197"

# Naming prefix
prefix = "greenscar"
