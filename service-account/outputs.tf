output "cluster_sa_id" {
    value = google_service_account.sa.unique_id
    description = "Service Account Unique ID"
}
output "cluster_sa_email" {
    value = google_service_account.sa.email
    description = "Service Account Email"
}
output "prefix"{
    value = var.prefix
    description = "The prefix you will use across projects"
}