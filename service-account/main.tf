
provider "google" {
  project     = var.project
}

resource "random_pet" "name" {
  length = 1
}

locals {
  prefix = "${var.prefix}-${random_pet.name.id}"
}
/*
* Create Service Account and grant the required permissions for cluster management.
*     TODO: go back through here & try to trim down permissions being given (to improve security)
*/
resource "google_service_account" "sa" {
    account_id   = "${local.prefix}-sa"
    display_name = "${local.prefix}-sa"
}


# https://cloud.google.com/kubernetes-engine/docs/how-to/iam
# Provides access to full management of clusters and their Kubernetes API objects.
resource "google_project_iam_member" "containerAdmin" {
    project = var.project
    role = "roles/container.admin"
    member = "serviceAccount:${google_service_account.sa.email}"
}

# Provides access to management of clusters.
resource "google_project_iam_member" "clusterAdmin" {
    project = var.project
    role = "roles/container.clusterAdmin"
    member = "serviceAccount:${google_service_account.sa.email}"
}

# To set a service account on nodes, you must also have the Service Account User role (roles/iam.serviceAccountUser) on the user-managed service account that your nodes will use.
resource "google_project_iam_member" "serviceAccountUser" {
    project = var.project
    role = "roles/iam.serviceAccountUser"
    member = "serviceAccount:${google_service_account.sa.email}"
}

resource "google_project_iam_member" "workloadIdentityUser" {
    project = var.project
    role   = "roles/iam.workloadIdentityUser"
    member = "serviceAccount:${google_service_account.sa.email}"
}

# To allow service_A to impersonate service_B, grant the Service Account Token Creator on B to A.
resource "google_project_iam_member" "serviceAccountTokenCreator" {
    project = var.project
    role   = "roles/iam.serviceAccountTokenCreator"
    member = "serviceAccount:${google_service_account.sa.email}"
}
